# Install

> These are the *official* download sources. For a list of unofficial sources, please visit the page *unofficial sources*.

## Install on GNU/Linux

The *somewhat* stable build of Spectral is available on Flathub.

```bash
flatpak install flathub org.eu.encom.spectral
```

Alternatively, you can fetch the latest Flatpak build [here](https://gitlab.com/api/v4/projects/5553000/jobs/artifacts/master/download?job=build-flatpak).

Or, you can download the AppImage [here](https://gitlab.com/api/v4/projects/5553000/jobs/artifacts/master/download?job=build-appimage).

### Fonts

If you have issues with the fonts, please install Google Noto Fonts or Roboto, either from your distribution's repository or download directly.

#### Arch Linux

```bash
pacman -S noto-fonts noto-fonts-emoji
```

#### Debian

```bash
apt install fonts-noto
```

## Install on Windows

There exist nightly builds from [AppVeyor](https://ci.appveyor.com/api/projects/BlackHat/spectral/artifacts/spectral.zip).

Spectral depends on OpenSSL 1.0.2. However, the OpenSSL libraries are not distributed by default due to U.S. cryptographic export restrictions. You need to install them manually from [this website](https://slproweb.com/products/Win32OpenSSL.html).


## Install on MacOS

The .dmg app bundle is available [here](https://gitlab.com/api/v4/projects/5553000/jobs/artifacts/master/download?job=build-osx).

## Install on FreeBSD

Please follow the instructions in *compile* page.
